# RECOPILACIÓN PREGUNTAS TEST LEGISLACIÓN  

## TEST 1  
### L’afirmació que la normativa de protecció de dades pot desplegar els seus efectes juntament amb altra normativa que reguli sectors específics de serveis i prestacions, és una afirmació:
- **Que és veritat perquè les dades d’una persona física són susceptibles de ser donades en diferents àmbits materials.**
### La informàtica forense és una disciplina criminalística que té com a funció la investigació de fets ocorreguts sobre sistemes informàtics i que es generen principalment en:
- **En l'àmbit jurídic i en l'àmbit privat/empresarial.**
### El concepte de jerarquia normativa estableix que:
- **Una norma de rang inferior no pot contravenir a una de rang superior.**
### A efectes de la normativa de protecció de dades, tot conjunt organitzat de dades de caràcter personal, qualsevol que sigui la seva forma o modalitat de creació, emmagatzematge, organització i accés, podria ser una definició possible per a un:
- **Un fitxer.**
### El primer instrument jurídic vinculant a nivell internacional fou:
- **El Conveni 108 del Consell d’Europa.**
### En una situació fictícia però excepcional, l’Escola d’Enginyeria de la UAB (EE) ha hagut de tractar dades personals seguint instruccions i ordres per compte del Rectorat (R) de la UAB, llavors:
- **L’EE és encarregat i el R responsable del tractament.**
### L’APDCAT són les sigles de:
- **Autoritat Catalana de Protecció de Dades.**
### Des del punt de vista de la LSSICE, els serveis de la societat de la informació són aquells que es presten:
- **A petició del destinatari, a distància i per via electrònica, a títol onerós, gairebé sempre.**  
---
## TEST 2
### Amb la pandèmia COVID-19, els professors de la UAB s’han queixat de la feina que els ha sotmès les noves formes de teletreball, amb qui dret estaria íntimament relacionat i s’ha exposat a la Llei 3/2018:
- **Dret a la desconnexió digital.**
### Amb la pandèmia COVID-19, el famós científic Oriol Mitjà (OM), per ordre del Institut Català de la Salut (ICS), ha reclutat una sèrie de voluntaris que, amb el seu consentiment, se’ls hi ha recollit les següents dades: edat, DNI, sexe, malalties que han patit, etc..., amb els efectes de fer una investigació sobre la vacuna contra el virus que es farà a l’Hospital Clínic (HC) de Barcelona, qui és qui als efectes del tractament de dades que se’ls hi fa als voluntaris?
- **HC és l’encarregat i l’ICS, responsable.**
### Amb la pandèmia COVID-19, el famós científic Oriol Mitjà, per ordre del Institut Català de la Salut, ha reclutat una sèrie de voluntaris que, amb el seu consentiment, se’ls hi ha recollit les següents dades: edat, DNI, sexe, malalties que han patit, etc..., amb els efectes de fer una investigació sobre la vacuna contra el virus a l’Hospital Clínic de Barcelona. Aquests voluntaris als efectes de les definicions del Reglament UE són:
- **Els voluntaris interessats.**
### A efectes de la normativa de protecció de dades, tot conjunt estructurat de dades de caràcter personal, accessibles segons criteris determinats, ja sigui centralitzat, descentralitzat o repartit de forma funcional o geogràfica, podria ser una definició possible per a:
- **Un fitxer.**
### A la colla castellera de Ganàpies de la UAB volen fer un tractament de dades i han demanat als seus components les dades individualitzades de l’alçada, el pes, dieta alimentícia, la pressió arterial i les pulsacions en repòs, de quin tipus de dades es tracten?
- **Dades biomètriques.**
### Amb la pandèmia COVID-19, als treballadors que s’incorporen presencialment a les fàbriques i els hi prenen la temperatura, es tracta d’una dada:
- **Relativa a la salut del treballador.**
### El NIU d’un estudiant a la UAB és:
- **Una dada personal seudonimitzada.**
### Amb la pandèmia COVID-19, els treballadors que s’incorporen presencialment a les fàbriques els hi prenen, sense el seu consentiment, la temperatura per a detectar si estan infectats o no, per tal de donar-los-en un passaport d’immunitat, s’hi infringeix la normativa sobre protecció de dades?
- **Sí. No solament s’infringeix la normativa de protecció de dades, sinó el dret fonamental a la intimitat.**
### Un ciutadà LGTBI, a través d’una piulada a Twitter, ha manifestat la seva orientació gay. El tractament d’aquesta dada seria:
- **No seria un tractament prohibit perquè se li aplicaria les circumstàncies d’excepció de l’art. 9 del Reglament Europeu.**
### En el panorama normatiu actual, la Llei Orgànica 3/2018, de 5 de desembre, se denomina així:
- **De Protecció de Dades Personals i Garantia dels Drets Digitals.**
---
## TEST 3
### Des del punt de vista de la Llei de Propietat Intel·lectual, el dret moral sobre una obra permet:
- **Decidir si una obra ha de ser divulgada i en quina forma, accedir a l'exemplar únic o rar de l'obra quan es trobi en poder d'un altre, exigir el reconeixement de la seva condició d'autor de l'obra.**
### En general, les normes jurídiques es caracteritzen per ser:
- **Generals, obligatòries i imperatives.**
### Quin de les següents NO és una de les etapes d'una anàlisi pericial?
- **Dissociació de les dades.**
### El dret que l'empresari pugui aplicar controls sobre les eines TIC que proporciona als empleats, sempre que s’informi degudament, es recull en:
- **L'Estatut dels Treballadors.**
### Segons les consideracions jurídiques generals, són fonts del Dret:
- **Tant les Lleis, els Costums com els Tractats Internacionals.**
### L'obligació d'informar sobre les cookies en les pàgines web que les utilitzen per a finalitats comercials o estadístiques neix en:
- **Llei de Serveis de la Societat de la Informació i Comerç Electrònic.**
### Sota la perspectiva de l'Esquema Nacional de Seguretat, la responsabilitat de definir els nivells de seguretat del servei recau sobre:
- **Responsable de Servei.**
### La LSSICE estableix els paràmetres a considerar en el comerç electrònic:
- **Tant indirecte off-line, com directe on-line.**
### El major volum de delictes informàtics que s'han produït a Espanya en els últims anys, amb gran diferència sobre la segona tipologia, es concentra en casos de:
- **Estafa.**
### Un blog d'un ciutadà particular està sotmès a la LSSICE en el cas que:
- **Tingui ingressos econòmics per publicitat en bànners.**
---
## TEST 4
### A diferencia de las evidencias analógicas, las evidencias digitales son:
- **Fácilmente alterables, fácilmente duplicables i volátiles.**
### Con el objeto de verificar que el almacenamiento de la información en los dispositivos de origen y de destino es idéntico, se recomienda hacer:
- **Una función hash.**
### Arturo, que es un aficionado y no una empresa, tiene un blog sobre la pesca con mosca y decide incrustar banners de Google Adwords a fin de obtener ingresos y pagar el coste de renovación del dominio. ¿está sujeto a la LSSI-CE?
- **Sí pues al obtener ingresos automáticamente debe cumplir con esa norma.**
### ¿Qué caracteriza el concepto de Derecho?
- **Posee una dimensión histórica, es un fenómeno social que se produce en el interior de un grupo de personas, tiene carácter normativo y regula el comportamiento de los individuos en una sociedad.**
### El derecho a que el empresario pueda aplicar controles sobre las herramientas TI que proporciona a los empleados, siempre que informe debidamente, se recoge en:
- **El Estatuto de los Trabajadores.**
### Un hacker que está provocando un ataque de denegación de servicio sobre una página web ajena, es decir, la deja indisponible, está incurriendo en un delito de:
- **Daños.**
### El ámbito de aplicación de la normativa de protección de datos será de aplicación a:
- **Cualquier tratamiento de datos personales automatizado y no automatizado contenidos o destinados a ser incluidos en un fichero.**
### El hecho de que una empresa utilice el software AutoCAD (requiere licencia) para la edición de planos en su versión “pirata” supone un incumplimiento de la:
- **Ley de Propiedad Intelectual.**
### Bajo el prisma de la LSSI-CE, es un servicio de la información:
- **La gestión de compras en la red por grupos de personas, el envío de comunicaciones comerciales, el suministro de información por vía telemática.**
### La jerarquía normativa establece que:
- **Una norma de rango inferior no puede ir contra lo que dice otra que tenga un rango superior, una ley especial prevalece frente a una ley general, ante dos normas de igual rango, la posterior en el tiempo deroga siempre a la norma anterior.**